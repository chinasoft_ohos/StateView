package com.github.nukc.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    // 纯UI组件，无法提供详细单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.github.nukc.sample", actualBundleName);
    }

}