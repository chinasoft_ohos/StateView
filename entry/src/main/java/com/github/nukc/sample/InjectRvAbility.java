package com.github.nukc.sample;

import com.github.nukc.stateview.StateView;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * InjectRvAbility
 *
 * @since 2021-04-26
 */
public class InjectRvAbility extends BaseAbility {
    @Override
    protected int setContentView() {
        setToolbarId(ResourceTable.Id_inject_rv_toolbar, StateView.class.getSimpleName());
        setInjectRv(true);
        return ResourceTable.Layout_ability_inject_rv;
    }

    @Override
    protected Component injectTarget() {
        ListContainer recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recycler_view);
        recyclerView.setLayoutManager(new DirectionalLayoutManager());
        final int count = 20;
        recyclerView.setItemProvider(new SampleAdapter(count, this));
        return recyclerView;
    }

    /**
     * SampleAdapter
     *
     * @since 2021-04-26
     */
    private static class SampleAdapter extends BaseItemProvider {
        private int mCount;
        private Context mContext;
        private Object defaultResult;

        SampleAdapter(int count, Context context) {
            mCount = count;
            mContext = context;
        }

        @Override
        public int getCount() {
            return mCount;
        }

        @Override
        public Object getItem(int position) {
            return defaultResult;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            Component view;
            if (component == null) {
                view = LayoutScatter.getInstance(mContext).parse(
                        ResourceTable.Layout_item_sample, componentContainer, false);
            } else {
                view = component;
            }
            Text mTextView;
            mTextView = (Text) view.findComponentById(ResourceTable.Id_text);
            mTextView.setText(position + "");
            return view;
        }
    }
}
