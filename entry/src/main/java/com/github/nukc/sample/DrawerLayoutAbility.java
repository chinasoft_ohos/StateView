package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.SlideDrawer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.security.SecureRandom;

/**
 * DrawerLayoutAbility
 *
 * @since 2021-04-26
 */
public class DrawerLayoutAbility extends Ability {
    EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    Runnable runnable;
    private StateView mStateView;
    private SlideDrawer drawer1;
    private DependentLayout drawerLayout;
    private Image fab;
    private int mProgress;
    private CustomToolbar toolbar;
    private AnimatorValue animatorValue;
    private SecureRandom random = new SecureRandom();
    private String defaultColor = "#FFFFFF";
    private Text textRun;
    private DirectionalLayout sendlayout;
    private DirectionalLayout shareLayout;
    private DirectionalLayout toolsLayout;
    private DirectionalLayout slideshowLayout;
    private DirectionalLayout galleryLayout;
    private DirectionalLayout importLayout;

    private Component coverView;

    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        setUIContent(ResourceTable.Layout_ability_drawer_layout);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_toolbar);
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        sendlayout = (DirectionalLayout) findComponentById(ResourceTable.Id_send_layout);
        shareLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_share_layout);
        toolsLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_tools_layout);
        slideshowLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_slideshow_layout);
        galleryLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_gallery_layout);
        importLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_import_layout);

        coverView = findComponentById(ResourceTable.Id_coverView);

        drawerLayout = (DependentLayout) findComponentById(ResourceTable.Id_drawer_layout);
        drawer1 = (SlideDrawer) findComponentById(ResourceTable.Id_draw1);

        findComponentById(ResourceTable.Id_main_drawer).setVisibility(Component.VISIBLE);
        Text text = (Text) findComponentById(ResourceTable.Id_main_title);
        textRun = (Text) findComponentById(ResourceTable.Id_TEXT1);
        text.setText(getAbilityName());
        final int marginLeft = 260;
        text.setMarginLeft(marginLeft);
        fab = (Image) findComponentById(ResourceTable.Id_search);
        setFab();
        mStateView = StateView.inject(drawerLayout);
        mStateView.setOnRetryClickListener(() -> {
        });
        mStateView.setDrawerLayout(true);
        final int marginBottom = 306;
        mStateView.setDrawerLayoutMarginBottom(marginBottom);
        setToolBar();
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                drawer1.setVisibility(Component.HIDE);
                drawer1.close();
                showAndDismissCover(false);
            }
        });
        setLayout();
        setLayout2();
    }

    private void setLayout2() {
        slideshowLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.close();
                drawer1.setVisibility(Component.HIDE);
                showAndDismissCover(false);
            }
        });
        galleryLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.close();
                drawer1.setVisibility(Component.HIDE);
                showAndDismissCover(false);
            }
        });
        importLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.close();
                drawer1.setVisibility(Component.HIDE);
                showAndDismissCover(false);
            }
        });
        coverView.setClickedListener(component -> {
            drawer1.close();
            drawer1.setVisibility(Component.HIDE);
            showAndDismissCover(false);
        });
    }

    private void setLayout() {
        sendlayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.close();
                drawer1.setVisibility(Component.HIDE);
                showAndDismissCover(false);
            }
        });
        shareLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.close();
                drawer1.setVisibility(Component.HIDE);
                showAndDismissCover(false);
            }
        });
        toolsLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.close();
                drawer1.setVisibility(Component.HIDE);
                showAndDismissCover(false);
            }
        });
    }

    private void setToolBar() {
        toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
            static final int caseTwo = 2;
            static final int caseThree = 3;
            @Override
            public void onMenuItemSelected(int position) {
                switch (position) {
                    case 0:
                        mStateView.showEmpty();
                        break;
                    case 1:
                        mStateView.showRetry();
                        break;
                    case caseTwo:
                        mStateView.showLoading();
                        break;
                    case caseThree:
                        mStateView.showContent();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onMenuNothingSelected() {

            }
        });
        toolbar.setItemOnClickInterface(() -> {
            drawer1.setVisibility(Component.VISIBLE);
            drawer1.open(SlideDrawer.SlideDirection.START);
            draweSlider();
            showAndDismissCover(true);
        });
    }

    private void setFab() {
        fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                final int duration = 1000;
                animatorValue = new AnimatorValue();
                animatorValue.setDuration(duration);
                final float[] animatorArray = {1f};
                animatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animator, float value) {
                        final int bottom = 100;
                        final int bottom2 = 32;
                        animatorArray[0] = 1 - value;
                        textRun.setMarginBottom((int) (-bottom * animatorArray[0]));
                        fab.setMarginBottom(bottom2 + (int) (bottom * value));
                    }
                });
                animatorValue.start();

                runnable = new Runnable() {
                    @Override
                    public void run() {
                        final int percent = 10;
                        final int maxPercent = 100;
                        mProgress += percent;
                        if (mProgress < maxPercent) {
                            eventHandler.postTask(this, generateDelay());
                        } else {
                            mProgress = 0;
                            final int bottom = -120;
                            fab.setMarginBottom(0);
                            textRun.setMarginBottom(bottom);
                        }
                    }
                };
                eventHandler.postTask(runnable, generateDelay());
            }
        });
    }

    private void draweSlider() {
        drawerLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                drawer1.setVisibility(Component.HIDE);
                drawer1.close();
                showAndDismissCover(false);
            }
        });
    }

    private int generateDelay() {
        final int bound = 1000;
        return random.nextInt(bound);
    }

    private void showAndDismissCover(boolean isShow) {
        coverView.setVisibility(isShow ? Component.VISIBLE : Component.HIDE);
    }
}
