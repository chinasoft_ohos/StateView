package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.sample.util.Toast;
import com.github.nukc.stateview.AnimatorProvider;
import com.github.nukc.stateview.LayoutRes;
import com.github.nukc.stateview.StateView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * BaseAbility
 *
 * @since 2021-04-26
 */
public abstract class BaseAbility extends Ability {
    private StateView mStateView;
    private int toolbarId;
    private String name;

    private boolean isRelative = false;
    private int relativeWidth;
    private int relativeHeight;
    private int relativeMarginTop;
    private boolean isInjectRv = false;

    public boolean isRelative() {
        return isRelative;
    }

    public void setRelative(boolean relative) {
        isRelative = relative;
    }
    public boolean isInjectRv() {
        return isInjectRv;
    }

    public void setInjectRv(boolean injectRv) {
        isInjectRv = injectRv;
    }

    public int getToolbarId() {
        return toolbarId;
    }

    /**
     * 设置toolbarId
     *
     * @param theToolbarId 工具栏id
     * @param theName      工具栏名称
     */
    public void setToolbarId(int theToolbarId, String theName) {
        this.toolbarId = theToolbarId;
        this.name = theName;
    }

    /**
     * 设置Relative的宽高
     *
     * @param width  宽度
     * @param height 高度
     */
    public void setRelativeWidthAndHeight(int width, int height) {
        relativeWidth = width;
        relativeHeight = height;
    }

    public void setRelativeMarginTop(int top) {
        relativeMarginTop = top;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(setContentView());
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        mStateView = StateView.inject(injectTarget());
        mStateView.setInjectRv(isInjectRv);
        setRelativeProperty();
        CustomToolbar toolbar = (CustomToolbar) findComponentById(getToolbarId());
        toolbar.setTitle(name);
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;
                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {
                    }
                });
            }
        });

        mStateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                // do something
                Toast.show(getContext(), "onRetryClick");
            }
        });
    }

    private void setRelativeProperty() {
        mStateView.setRelative(isRelative);
        if (isRelative) {
            mStateView.setRelativeWidthAndHeight(relativeWidth, relativeHeight);
            mStateView.setRelativeMarginTop(relativeMarginTop);
        }
    }

    /**
     * 设置内容视图
     *
     * @return int 返回设置内容视图
     */
    protected abstract @LayoutRes
    int setContentView();

    /**
     * 注入目标
     *
     * @return Component 组件
     */
    protected abstract Component injectTarget();

    /**
     * 设置动画
     *
     * @param animator 动画提供者
     */
    protected void setAnimator(AnimatorProvider animator) {
        if (mStateView != null) {
            mStateView.setAnimatorProvider(animator);
        }
    }
}
