package com.github.nukc.sample;

import com.github.nukc.stateview.StateView;
import com.github.nukc.stateview.animations.FadeScaleAnimatorProvider;
import com.github.nukc.stateview.animations.FlipAnimatorProvider;
import com.github.nukc.stateview.animations.SlideAnimatorProvider;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

/**
 * AnimatorAbility
 *
 * @since 2021-04-26
 */
public class AnimatorAbility extends Ability {
    private StateView mStateView;
    private int mStatus = 0;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_animator);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        mStateView = (StateView) findComponentById(ResourceTable.Id_stateView);
        mStateView.setEmptyResource(ResourceTable.Layout_view_empty);
        setBtn();
        mStateView.setAnimator(true);

        mStateView.setOnInflateListener((viewType, view) -> {
            if (viewType == StateView.EMPTY) {
                // set text or other
                ComponentContainer emptyView = (ComponentContainer) view;
                Text tvMessage = (Text) emptyView.findComponentById(ResourceTable.Id_tv_message);
                Image ivState = (Image) emptyView.findComponentById(ResourceTable.Id_iv_state);
                tvMessage.setText("custom message");
                ivState.setImageAndDecodeBounds(ResourceTable.Media_retry);
            }
        });
    }

    private void setBtn() {
        findComponentById(ResourceTable.Id_btnFadeScale).setClickedListener(component -> {
            mStateView.setAnimatorProvider(new FadeScaleAnimatorProvider());
            showAnimationView();
        });
        findComponentById(ResourceTable.Id_btnFlip).setClickedListener(component -> {
            mStateView.setAnimatorProvider(new FlipAnimatorProvider());
            showAnimationView();
        });
        findComponentById(ResourceTable.Id_btnSlide).setClickedListener(component -> {
            SlideAnimatorProvider slideAnimatorProvider = new SlideAnimatorProvider();
            mStateView.setAnimatorProvider(slideAnimatorProvider);
            showAnimationView();
        });
    }

    private void showAnimationView() {
        switch (mStatus) {
            case 0:
                mStateView.showLoading();
                mStatus = 1;
                break;
            case 1:
                mStateView.showEmpty();
                mStatus = 0;
                break;
            default:
                break;
        }
    }
}
