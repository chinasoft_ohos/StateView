package com.github.nukc.sample;


import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;
import com.github.nukc.stateview.animations.FadeScaleAnimatorProvider;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;


/**
 * WrapAbility
 *
 * @since 2021-04-26
 */
public class WrapAbility extends Ability {

    private StateView mStateView;
    private Text text;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_inject);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        CustomToolbar toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_injecttoolbar);
        toolbar.setTitle(StateView.class.getSimpleName());
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });
        setTextProperty();
        mStateView = StateView.wrap(text);
        mStateView.setWrap(true);
        mStateView.setAnimatorProvider(new FadeScaleAnimatorProvider());

        mStateView.setOnRetryClickListener(() -> mStateView.showRetry());
    }

    private void setTextProperty() {
        text = (Text) findComponentById(ResourceTable.Id_text_view);
        final int dividerNum = 2;
        final int toolbarHeight = 180;
        int screenWidth = DisplayManager.getInstance().getDefaultDisplay(getContext()).
                get().getRealAttributes().width;
        int screenHeight = DisplayManager.getInstance().getDefaultDisplay(getContext()).
                get().getRealAttributes().height - toolbarHeight;
        text.setMarginsTopAndBottom(screenHeight / dividerNum, screenHeight / dividerNum);
        text.setMarginsLeftAndRight(screenWidth / dividerNum, screenWidth / dividerNum);
    }
}
