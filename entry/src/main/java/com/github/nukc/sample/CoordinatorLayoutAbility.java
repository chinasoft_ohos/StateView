package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import ohos.agp.colors.RgbColor;

import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.multimodalinput.event.TouchEvent;

/**
 * CoordinatorLayoutAbility
 *
 * @since 2021-04-26
 */
public class CoordinatorLayoutAbility extends Ability {
    private static final int MAX_HEIGHT = 300;
    private StateView mStateView;
    private DirectionalLayout coordinator;
    private CustomToolbar toolbar;
    private Text titleText;
    private final int minHeight = 180;
    private final int minMarginTop = 0;
    private final int minTitleSize = 60;
    private final int maxPaddingLeft = 180;
    private final int minTitleMarginTop = 40;
    private final int maxMarginTop = 180;
    private final int maxTitleSize = 90;
    private final int minPaddingLeft = 60;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_coordinator_layout);
        coordinator = (DirectionalLayout) findComponentById(ResourceTable.Id_coordinator);
        titleText = (Text) findComponentById(ResourceTable.Id_title_text);
        titleText.setHeight(MAX_HEIGHT);
        titleText.setText(StateView.class.getSimpleName());
        Image back = (Image) findComponentById(ResourceTable.Id_main_back);
        back.setVisibility(Component.VISIBLE);
        back.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });

        toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_coordinator_toolbar);
        Image imageMore = (Image) toolbar.findComponentById(ResourceTable.Id_main_menu_image);
        imageMore.setImageAndDecodeBounds(ResourceTable.Media_ic_more2);
        toolbar.setTitle("");
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        setCoordinator();
        setToolbar();
        mStateView = StateView.inject(findComponentById(ResourceTable.Id_coordinator_text));
        mStateView.setOnRetryClickListener(() -> mStateView.showContent());
    }

    private void setToolbar() {
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;
                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });
    }

    private void setCoordinator() {
        coordinator.setTouchEventListener(new Component.TouchEventListener() {
            float startY;
            float endY;
            boolean isMoved = false;

            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                switch (touchEvent.getAction()) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        startY = touchEvent.getPointerScreenPosition(0).getY();
                        break;
                    case TouchEvent.PRIMARY_POINT_UP:
                        if (isMoved) {
                            setProperties(startY, endY);
                        }
                        break;
                    case TouchEvent.POINT_MOVE:
                        isMoved = true;
                        endY = touchEvent.getPointerPosition(0).getY();
                        setProperties(startY, endY);
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
    }

    private void setProperties(float startY, float endY) {
        int finalHeight;
        int finalMarginTop;
        int finalTitleSize;
        int finalPaddingLeft;
        int titleHeight = titleText.getHeight();
        int titleMarginTop = titleText.getMarginTop();
        int titleSize = titleText.getTextSize();
        int titlePaddingLeft = titleText.getPaddingLeft();
        if (startY > endY) {
            finalHeight = (int) (titleHeight * (1 - Math.abs(startY - endY) / startY));
            if (finalHeight <= minHeight) {
                finalHeight = minHeight;
            }
            finalMarginTop = (int) (titleMarginTop * (1 - Math.abs(startY - endY) / startY));
            if (finalMarginTop <= minMarginTop) {
                finalMarginTop = minMarginTop;
            }
            finalTitleSize = (int) (titleSize * (1 - Math.abs(startY - endY) / startY));
            if (finalTitleSize <= minTitleSize) {
                finalTitleSize = minTitleSize;
            }
            finalPaddingLeft = (int) (titlePaddingLeft * (1 + Math.abs(startY - endY) / startY));
            if (finalPaddingLeft >= maxPaddingLeft) {
                finalPaddingLeft = maxPaddingLeft;
            }
        } else {
            finalHeight = (int) (titleHeight * (1 + Math.abs(endY - startY) / startY));
            if (finalHeight >= MAX_HEIGHT) {
                finalHeight = MAX_HEIGHT;
            }
            if (titleMarginTop <= 0) {
                titleMarginTop = minTitleMarginTop;
            }
            finalMarginTop = (int) (titleMarginTop * (1 + Math.abs(endY - startY) / startY));
            if (finalMarginTop >= maxMarginTop) {
                finalMarginTop = maxMarginTop;
            }
            finalTitleSize = (int) (titleSize * (1 + Math.abs(endY - startY) / startY));
            if (finalTitleSize >= maxTitleSize) {
                finalTitleSize = maxTitleSize;
            }
            finalPaddingLeft = (int) (titlePaddingLeft * (1 - Math.abs(endY - startY) / startY));
            if (finalPaddingLeft <= minPaddingLeft) {
                finalPaddingLeft = minPaddingLeft;
            }
        }
        setTitleText(finalHeight,finalMarginTop,finalTitleSize,finalPaddingLeft);
    }
    private void setTitleText(int finalHeight,int finalMarginTop,int finalTitleSize,int finalPaddingLeft) {
        titleText.setHeight(finalHeight);
        titleText.setMarginTop(finalMarginTop);
        titleText.setTextSize(finalTitleSize);
        titleText.setPaddingLeft(finalPaddingLeft);
    }
}
