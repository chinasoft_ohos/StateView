package com.github.nukc.sample;

import com.github.nukc.stateview.StateView;

import ohos.agp.components.Component;

/**
 * RefreshAbility
 *
 * @since 2021-04-26
 */
public class RefreshAbility extends BaseAbility {

    @Override
    protected int setContentView() {
        setToolbarId(ResourceTable.Id_refresh_toolbar, StateView.class.getSimpleName());
        return ResourceTable.Layout_ability_refresh;
    }

    @Override
    protected Component injectTarget() {
        return findComponentById(ResourceTable.Id_refresh_text_view);
    }
}
