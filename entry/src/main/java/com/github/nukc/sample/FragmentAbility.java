package com.github.nukc.sample;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;

/**
 * FragmentAbility
 *
 * @since 2021-04-26
 */
public class FragmentAbility extends FractionAbility {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fragment);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        FractionScheduler fractionScheduler = getFractionManager().startFractionScheduler();
        fractionScheduler.replace(ResourceTable.Id_content,new InjectFragment())
                .submit();
    }
}
