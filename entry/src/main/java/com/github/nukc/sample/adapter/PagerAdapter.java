/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.nukc.sample.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.List;

/**
 * PagerAdapter
 *
 * @since 2021-04-26
 */
public class PagerAdapter extends PageSliderProvider {
    private List<Component> pageviews;

    /** 构造PagerAdapter
     *
     * @param pageviews pageviews
     * @param context context
     */
    public PagerAdapter(List<Component> pageviews, Context context) {
        this.pageviews = pageviews;
    }

    @Override
    public int getCount() {
        return pageviews.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        componentContainer.addComponent(pageviews.get(position));
        return pageviews.get(position);
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer container, int position, Object object) {
        container.removeComponent(pageviews.get(position));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }
}
