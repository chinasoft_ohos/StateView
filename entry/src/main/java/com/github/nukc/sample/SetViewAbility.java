package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.sample.util.Toast;
import com.github.nukc.stateview.StateView;
import com.github.nukc.stateview.animations.SlideAnimatorProvider;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * SetViewAbility
 *
 * @since 2021-04-26
 */
public class SetViewAbility extends Ability {
    private StateView mStateView;
    private CustomToolbar toolbar;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_inject);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_injecttoolbar);
        StackLayout injectStack = (StackLayout) findComponentById(ResourceTable.Id_injectstack);
        toolbar.setTitle(StateView.class.getSimpleName());
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        setToolbar();

        mStateView = StateView.inject(injectStack);
        mStateView.setEmptyView(true);
        SlideAnimatorProvider slideAnimatorProvider = new SlideAnimatorProvider();
        mStateView.setAnimatorProvider(slideAnimatorProvider);
        mStateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                mStateView.showContent();
                Toast.show(getContext(), "onRetryClick");
            }
        });

        Component emptyView = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_view_empty, null, false);
        Text tvMessage = (Text) emptyView.findComponentById(ResourceTable.Id_tv_message);
        if (tvMessage != null) {
            tvMessage.setText("Run setEmptyView");
        }
        mStateView.setEmptyResource(ResourceTable.Layout_empty_view_retry);
        mStateView.setRetryResource(ResourceTable.Layout_view_retry31);
    }

    private void setToolbar() {
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });
    }
}
