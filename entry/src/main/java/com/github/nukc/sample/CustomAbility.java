package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * CustomAbility
 *
 * @since 2021-04-26
 */
public class CustomAbility extends Ability {

    private StateView mStateView;

    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        setUIContent(ResourceTable.Layout_ability_custom);

        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        DirectionalLayout linearLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_linear_layout);
        CustomToolbar toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_customtoolbar);
        toolbar.setTitle(StateView.class.getSimpleName());
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        mStateView = StateView.inject(linearLayout);
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });

        mStateView = StateView.inject(linearLayout);
        mStateView.setEmptyResource(ResourceTable.Layout_view_empty);
        mStateView.setRetryResource(ResourceTable.Layout_view_retry);
        mStateView.setOnRetryClickListener(() -> {
        });
    }
}
