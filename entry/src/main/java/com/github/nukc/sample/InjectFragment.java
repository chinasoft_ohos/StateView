package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

/**
 * InjectFragment
 *
 * @since 2021-04-26
 */
public class InjectFragment extends Fraction {

    private StateView mStateView;

    @Override
    public Component onComponentAttached(LayoutScatter inflater, ComponentContainer container,
                                         Intent savedInstanceState) {
        Component view = inflater.getInstance(container.getContext()).parse(
                ResourceTable.Layout_fragment_inject, container, false);
        CustomToolbar toolbar = (CustomToolbar) view.findComponentById(ResourceTable.Id_frag_toolbar);

        DirectionalLayout injectFragment = (DirectionalLayout) view.findComponentById(ResourceTable.Id_inject_fragment);
        toolbar.setTitle("inject fragment");

        Text text = (Text) toolbar.findComponentById(ResourceTable.Id_main_title);
        text.setTextColor(new Color(Color.getIntColor("#FF000000")));

        Image image = (Image) toolbar.findComponentById(ResourceTable.Id_main_menu_image);
        image.setImageAndDecodeBounds(ResourceTable.Media_ic_more2);
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    @Override
                    public void onMenuItemSelected(int position) {
                        final int caseTwo = 2;
                        final int caseThree = 3;
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }
                    @Override
                    public void onMenuNothingSelected() {
                    }
                });
            }
        });

        mStateView = StateView.inject(injectFragment);

        mStateView.setOnRetryClickListener(() -> {
        });
        return view;
    }
}
