package com.github.nukc.sample;

import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;
import com.github.nukc.stateview.animations.SlideAnimatorProvider;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.DisplayManager;

/**
 * ConstraintLayoutAbility
 *
 * @since 2021-04-26
 */
public class ConstraintLayoutAbility extends Ability {
    private StateView mStateView;

    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        setUIContent(ResourceTable.Layout_ability_constraint_layout);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        CustomToolbar toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_consttoolbar);
        toolbar.setTitle(StateView.class.getSimpleName());
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {
                    }
                });
            }
        });

        setSateView();
    }

    private void setSateView() {
        final int dividerNum = 2;
        final int toolbarHeight = 180;
        int marginBottom;
        int marginTop;
        final int constraintWidth = 700;
        final int constraintHeight = 700;
        final int halfWidthOfComponent = 350;
        int screenHeight = DisplayManager.getInstance().getDefaultDisplay(getContext()).
                get().getRealAttributes().height;
        int screenWidth = DisplayManager.getInstance().getDefaultDisplay(getContext()).
                get().getRealAttributes().width;
        if (screenHeight > screenWidth) {
            marginBottom = toolbarHeight;
            marginTop = screenHeight / dividerNum - constraintHeight / dividerNum - toolbarHeight;
        } else {
            marginBottom = 0;
            marginTop = toolbarHeight;
        }
        Text text = (Text) findComponentById(ResourceTable.Id_text);
        mStateView = StateView.inject(text);
        mStateView.setConstraintLayout(true);
        mStateView.setConstraintMarginBottomAndTop(marginBottom, marginTop);
        mStateView.setConstraintWidthAndHeight(constraintWidth, constraintHeight);
        SlideAnimatorProvider slideAnimatorProvider = new SlideAnimatorProvider();
        slideAnimatorProvider.setConstraint(true);
        slideAnimatorProvider.setHalfWidthOfComponent(halfWidthOfComponent);
        mStateView.setAnimatorProvider(slideAnimatorProvider);
        mStateView.setEmptyResource(ResourceTable.Layout_view_empty);
        mStateView.setRetryResource(ResourceTable.Layout_view_retry);
        mStateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                mStateView.showRetry();
            }
        });
    }
}
