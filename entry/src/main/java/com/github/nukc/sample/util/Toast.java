/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.nukc.sample.util;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Toast
 *
 * @since 2021-03-01
 */
public class Toast {
    private static final int LENGTH_LONG = 4000;
    private static final int LENGTH_SHORT = 2000;

    /**
     * Toast显示的位置枚举类型
     *
     * @since 2021-03-01
     */
    public enum ToastLayout {
        /**
         * 系统默认
         */
        DEFAULT,
        /**
         * 中间
         */
        CENTER,
        /**
         * 顶部
         */
        TOP,
        /**
         * 底部
         */
        BOTTOM,
    }
    
    private Toast() {
    }

    /**
     * 显示短时间的Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void showShort(Context context, String content) {
        createTost(context, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示短时Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void showShort(Context context, int content) {
        createTost(context, getString(context, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示长时间的Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void showLong(Context context, String content) {
        createTost(context, content, LENGTH_LONG, ToastLayout.DEFAULT);
    }

    /**
     * 显示长时Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void showLong(Context context, int content) {
        createTost(context, getString(context, content), LENGTH_LONG, ToastLayout.DEFAULT);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void show(Context context, String content) {
        createTost(context, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param duration 时间 ms
     */
    public static void show(Context context, String content, int duration) {
        createTost(context, content, duration, ToastLayout.DEFAULT);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param layout 位置
     */
    public static void show(Context context, String content, ToastLayout layout) {
        createTost(context, content, LENGTH_SHORT, layout);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param duration 时间
     * @param layout 位置
     */
    public static void show(Context context, String content, int duration, ToastLayout layout) {
        createTost(context, content, duration, layout);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void show(Context context, int content) {
        createTost(context, getString(context, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param duration 时间 ms
     */
    public static void show(Context context, int content, int duration) {
        createTost(context, getString(context, content), duration, ToastLayout.DEFAULT);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param layout 位置
     */
    public static void show(Context context, int content, ToastLayout layout) {
        createTost(context, getString(context, content), LENGTH_SHORT, layout);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param duration 时间 ms
     * @param layout 位置
     */
    public static void show(Context context, int content, int duration, ToastLayout layout) {
        createTost(context, getString(context, content), duration, layout);
    }

    private static void createTost(Context context, String content, int duration, ToastLayout layout) {
        DirectionalLayout toastLayout = new DirectionalLayout(context);
        DirectionalLayout.LayoutConfig textConfig = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        Text text = new Text(context);
        text.setText(content);
        text.setTextColor(new Color(Color.getIntColor("#000000")));
        final int paddingLeft = 16;
        final int paddingTop = 12;
        final int paddingRight = 16;
        final int paddingBottom = 12;
        final int textSize = 15;
        final float radius = 20f;
        final int offSetOfY = 20;
        text.setPadding(vp2px(context, paddingLeft), vp2px(context, paddingTop),
                vp2px(context, paddingRight), vp2px(context, paddingBottom));
        text.setTextSize(vp2px(context, textSize));
        text.setBackground(buildDrawableByColorRadius(Color.getIntColor("#F5F5F5"), vp2px(context, radius)));
        text.setLayoutConfig(textConfig);
        text.setTextAlignment(TextAlignment.CENTER);
        toastLayout.addComponent(text);
        int mLayout = LayoutAlignment.BOTTOM;
        switch (layout) {
            case TOP:
                mLayout = LayoutAlignment.TOP;
                break;
            case BOTTOM:
                mLayout = LayoutAlignment.BOTTOM;
                break;
            case CENTER:
                mLayout = LayoutAlignment.CENTER;
                break;
            default:
                break;
        }
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(toastLayout);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(mLayout);
        toastDialog.setTransparent(true);
        toastDialog.setOffset(0,offSetOfY);
        toastDialog.setDuration(duration);
        toastDialog.show();
    }

    private static ohos.agp.components.element.Element buildDrawableByColorRadius(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private static String getString(Context context, int resId) {
        try {
            return context.getResourceManager().getElement(resId).getString();
        } catch (NotExistException | WrongTypeException | IOException e) {
            e.getMessage();
        }
        return "";
    }

    private static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}
