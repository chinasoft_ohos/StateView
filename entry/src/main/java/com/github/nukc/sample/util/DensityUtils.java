/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.nukc.sample.util;

import static ohos.agp.components.AttrHelper.getDensity;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.io.File;
import java.util.Optional;

/**
 * DensityUtils
 *
 * @since 2021-04-26
 */
public class DensityUtils {

    private static Display display;

    /**
     * fp 转 Px
     *
     * @param context context
     * @param fp fp
     * @return int
     */
    public static int fpToPx(final Context context, final float fp) {
        return AttrHelper.fp2px(fp, getDensity(context));
    }

    /**
     * px 转 Fp
     *
     * @param context context
     * @param px px
     * @return int
     */
    public static int pxToFp(final Context context, final float px) {
        return Math.round(px * getDensity(context));
    }

    /**
     * 通过path返回字体
     *
     * @param path path
     * @return 字体
     */
    public static Font font(File path) {
        Font.Builder font = new Font.Builder(path);
        return font.build();
    }

    /**
     * 通过name返回字体
     *
     * @param name name
     * @return 字体
     */
    public static Font font(String name) {
        Font.Builder font = new Font.Builder(name);
        return font.build();
    }

    /**
     * 获取当前设备属性
     *
     * @param context context
     * @return 当前设备属性
     */
    public static Display getDeviceAttr(Context context) {
        Optional<Display> optional = DisplayManager.getInstance().getDefaultDisplay(context);
        if (optional.isPresent()) {
            display = optional.get();
        }
        return display;
    }

    /**
     * 屏幕宽
     *
     * @param context context
     * @return 屏幕宽度
     */
    public static int getDisplayWidth(Context context) {
        return getDeviceAttr(context).getRealAttributes().width;
    }

    /**
     * 屏幕高
     *
     * @param context context
     * @return 屏幕高度
     */
    public static int getDisplayHeight(Context context) {
        return getDeviceAttr(context).getRealAttributes().height;
    }

    /**
     * rgb颜色
     *
     * @param color color
     * @return RgbColor
     */
    private RgbColor getRgbColor(Color color) {
        return RgbColor.fromArgbInt(color.getValue());
    }
}