package com.github.nukc.sample;

import com.github.nukc.sample.util.Toast;
import com.github.nukc.stateview.StateView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

/**
 * DefaultAbility
 *
 * @since 2021-04-26
 */
public class DefaultAbility extends Ability implements Component.ClickedListener {

    private StateView mStateView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_default);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        mStateView = (StateView) findComponentById(ResourceTable.Id_stateView);
        findComponentById(ResourceTable.Id_btnInEmpty).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnInRetry).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnRemove).setClickedListener(this);


        mStateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
            @Override
            public void onRetryClick() {
                Toast.show(getContext(), "retry click");
            }
        });

        findComponentById(ResourceTable.Id_btnTest).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toast.show(getContext(), "can click");
            }
        });
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btnInEmpty:
                findComponentById(ResourceTable.Id_hello).setVisibility(Component.INVISIBLE);
                mStateView.showEmpty();
                break;
            case ResourceTable.Id_btnInRetry:
                findComponentById(ResourceTable.Id_hello).setVisibility(Component.INVISIBLE);
                mStateView.showRetry();
                break;
            case ResourceTable.Id_btnRemove:
                findComponentById(ResourceTable.Id_hello).setVisibility(Component.VISIBLE);
                mStateView.showContent();
                break;
            default:
                break;
        }
    }
}
