package com.github.nukc.sample;

import com.github.nukc.sample.adapter.PagerAdapter;
import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewPager2Ability
 *
 * @since 2021-04-26
 */
public class ViewPager2Ability extends Ability {
    private TabList tabList;
    private List<Component> pageviews;
    private StateView mStateView;
    private StateView mStateView2;
    private StateView mStateView3;
    private CustomToolbar toolbar;
    private CustomToolbar toolbar2;
    private CustomToolbar toolbar3;
    private String title = "inject fragment";

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_view_pager2);
        initTabList();
        final int tabMargin = 16;
        tabList.setTabMargin(tabMargin); // 设置两个Tab之间的间距
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DirectionalLayout dependentLayout1 = (DirectionalLayout) layoutScatter.parse(
                ResourceTable.Layout_fragment_inject, null, false);
        DirectionalLayout dependentLayout2 = (DirectionalLayout) layoutScatter.parse(
                ResourceTable.Layout_fragment_inject, null, false);
        DirectionalLayout dependentLayout3 = (DirectionalLayout) layoutScatter.parse(
                ResourceTable.Layout_fragment_inject, null, false);
        toolbar = (CustomToolbar) dependentLayout1.findComponentById(ResourceTable.Id_frag_toolbar);
        toolbar.setTitle(title);
        toolbar.setViewPager2(true);
        toolbar2 = (CustomToolbar) dependentLayout2.findComponentById(ResourceTable.Id_frag_toolbar);
        toolbar2.setTitle(title);
        toolbar2.setViewPager2(true);
        toolbar3 = (CustomToolbar) dependentLayout3.findComponentById(ResourceTable.Id_frag_toolbar);
        toolbar3.setTitle(title);
        toolbar3.setViewPager2(true);
        DirectionalLayout injectFragment = (DirectionalLayout) dependentLayout1.findComponentById(
                ResourceTable.Id_inject_fragment);
        DirectionalLayout injectFragment2 = (DirectionalLayout) dependentLayout2.findComponentById(
                ResourceTable.Id_inject_fragment);
        DirectionalLayout injectFragment3 = (DirectionalLayout) dependentLayout3.findComponentById(
                ResourceTable.Id_inject_fragment);
        setToolBars();
        mStateView = StateView.inject(injectFragment);
        mStateView.setOnRetryClickListener(() -> {
        });
        mStateView2 = StateView.inject(injectFragment2);
        mStateView2.setOnRetryClickListener(() -> {
        });
        mStateView3 = StateView.inject(injectFragment3);
        mStateView3.setOnRetryClickListener(() -> {
        });
        pageviews = new ArrayList<Component>();
        pageviews.add(dependentLayout1);
        pageviews.add(dependentLayout2);
        pageviews.add(dependentLayout3);
        setTabAndPageSlider();
    }

    private void setTabAndPageSlider() {
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_view_pager);
        pageSlider.setProvider(new PagerAdapter(pageviews, getContext()));

        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                pageSlider.setCurrentPage(tab.getPosition());
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {
            }
        });
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                tabList.selectTabAt(i);
            }
        });
    }

    private void initTabList() {
        tabList = (TabList) findComponentById(ResourceTable.Id_tab_layout);
        TabList.Tab tab1 = tabList.new Tab(getContext());
        tab1.setText("0");
        TabList.Tab tab2 = tabList.new Tab(getContext());
        tab2.setText("1");
        TabList.Tab tab3 = tabList.new Tab(getContext());
        tab3.setText("2");
        tabList.addTab(tab1);
        tabList.addTab(tab2);
        tabList.addTab(tab3);
        tabList.selectTab(tab1);
    }

    private void setToolBars() {
        final String defaultColor = "#FF000000";
        Text text = (Text) toolbar.findComponentById(ResourceTable.Id_main_title);
        text.setTextColor(new Color(Color.getIntColor(defaultColor)));
        Image image = (Image) toolbar.findComponentById(ResourceTable.Id_main_menu_image);
        image.setImageAndDecodeBounds(ResourceTable.Media_ic_more2);

        Text text2 = (Text) toolbar2.findComponentById(ResourceTable.Id_main_title);
        text2.setTextColor(new Color(Color.getIntColor(defaultColor)));
        Image image2 = (Image) toolbar2.findComponentById(ResourceTable.Id_main_menu_image);
        image2.setImageAndDecodeBounds(ResourceTable.Media_ic_more2);

        Text text3 = (Text) toolbar3.findComponentById(ResourceTable.Id_main_title);
        text3.setTextColor(new Color(Color.getIntColor(defaultColor)));
        Image image3 = (Image) toolbar3.findComponentById(ResourceTable.Id_main_menu_image);
        image3.setImageAndDecodeBounds(ResourceTable.Media_ic_more2);
        setToolBar();
        setToolBar2();
        setToolBar3();
    }

    private void setToolBar3() {
        toolbar3.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar3.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView3.showEmpty();
                                break;
                            case 1:
                                mStateView3.showRetry();
                                break;
                            case caseTwo:
                                mStateView3.showLoading();
                                break;
                            case caseThree:
                                mStateView3.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });
    }

    private void setToolBar2() {
        toolbar2.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar2.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView2.showEmpty();
                                break;
                            case 1:
                                mStateView2.showRetry();
                                break;
                            case caseTwo:
                                mStateView2.showLoading();
                                break;
                            case caseThree:
                                mStateView2.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });
    }

    private void setToolBar() {
        toolbar.setPopupItemOnClickInterface(new CustomToolbar.PopupItemOnClickInterface() {
            @Override
            public void onItemClick() {
                toolbar.setMenuOnItemSelectedListener(new CustomToolbar.OnMenuItemSelectedListener() {
                    static final int caseTwo = 2;
                    static final int caseThree = 3;

                    @Override
                    public void onMenuItemSelected(int position) {
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }

                    @Override
                    public void onMenuNothingSelected() {

                    }
                });
            }
        });
    }

}
