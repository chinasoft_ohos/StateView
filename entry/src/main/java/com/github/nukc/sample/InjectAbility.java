package com.github.nukc.sample;


import com.github.nukc.sample.custom.CustomToolbar;
import com.github.nukc.stateview.StateView;
import com.github.nukc.stateview.animations.SlideAnimatorProvider;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * InjectAbility
 *
 * @since 2021-04-26
 */
public class InjectAbility extends Ability {
    private StateView mStateView;

    @Override
    protected void onStart(Intent savedInstanceState) {
        super.onStart(savedInstanceState);
        setUIContent(ResourceTable.Layout_ability_inject);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        CustomToolbar toolbar = (CustomToolbar) findComponentById(ResourceTable.Id_injecttoolbar);
        ShapeElement background = new ShapeElement(getContext(), ResourceTable.Graphic_ms__drawable);
        background.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#4050B5")));
        toolbar.setBackground(background);

        StackLayout dependentLayout = (StackLayout) findComponentById(ResourceTable.Id_injectstack);
        Text text = (Text) toolbar.findComponentById(ResourceTable.Id_main_title);
        text.setText(StateView.class.getSimpleName());
        toolbar.setPopupItemOnClickInterface(() -> toolbar.setMenuOnItemSelectedListener(
                new CustomToolbar.OnMenuItemSelectedListener() {
                    @Override
                    public void onMenuItemSelected(int position) {
                        final int caseTwo = 2;
                        final int caseThree = 3;
                        switch (position) {
                            case 0:
                                mStateView.showEmpty();
                                break;
                            case 1:
                                mStateView.showRetry();
                                break;
                            case caseTwo:
                                mStateView.showLoading();
                                break;
                            case caseThree:
                                mStateView.showContent();
                                break;
                            default:
                                break;
                        }
                    }
                    @Override
                    public void onMenuNothingSelected() {
                    }
                }));

        mStateView = StateView.inject(dependentLayout);
        mStateView.setInject(true);
        SlideAnimatorProvider slideAnimatorProvider = new SlideAnimatorProvider();
        mStateView.setAnimatorProvider(slideAnimatorProvider);
        mStateView.setOnRetryClickListener(() -> mStateView.showRetry());
    }
}
