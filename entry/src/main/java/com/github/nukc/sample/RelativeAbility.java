package com.github.nukc.sample;

import com.github.nukc.stateview.StateView;
import com.github.nukc.stateview.animations.SlideAnimatorProvider;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.service.DisplayManager;

/**
 * RelativeAbility
 *
 * @since 2021-04-26
 */
public class RelativeAbility extends BaseAbility {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        final int dividerNum = 2;
        final int distance = 150;
        SlideAnimatorProvider slideAnimatorProvider = new SlideAnimatorProvider();
        slideAnimatorProvider.setRelative(true);
        int halfWidth = DisplayManager.getInstance().
                getDefaultDisplay(getContext()).get().getRealAttributes().width / dividerNum - distance;
        slideAnimatorProvider.setHalfWidthOfComponent(halfWidth);
        setAnimator(slideAnimatorProvider);
    }

    @Override
    protected Component injectTarget() {
        return findComponentById(ResourceTable.Id_btn);
    }

    @Override
    protected int setContentView() {
        setRelative(true);
        final int width = 600;
        final int height = 600;
        final int marginTop = 300;
        setRelativeWidthAndHeight(width, height);
        setRelativeMarginTop(marginTop);
        setToolbarId(ResourceTable.Id_relattoolbar, StateView.class.getSimpleName());
        return ResourceTable.Layout_ability_relative;
    }

}
