/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.nukc.sample.adapter;

import com.github.nukc.sample.ResourceTable;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.Collections;
import java.util.List;

/**
 * ToolbarAdapter
 *
 * @since 2021-04-26
 */
public class ToolbarAdapter extends BaseItemProvider {
    private List<String> mColorList;
    private Context context;

    /** 构造ToolbarAdapter
     *
     * @param context context
     * @param clolorList clolorList
     */
    public ToolbarAdapter(Context context, List<String> clolorList) {
        this.mColorList = clolorList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mColorList == null ? 0 : mColorList.size();
    }

    @Override
    public Object getItem(int position) {
        if (mColorList != null && position >= 0 && position < mColorList.size()) {
            return mColorList.get(position);
        }
        return Collections.emptyList();
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_menu_list, null, false);
        } else {
            cpt = convertComponent;
        }
        String color = mColorList.get(position);

        Text text = (Text) cpt.findComponentById(ResourceTable.Id_text_item);
        text.setText(color);

        return cpt;
    }
}
