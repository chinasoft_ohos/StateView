package com.github.nukc.sample;


import com.github.nukc.stateview.StateView;

import ohos.agp.components.Component;

/**
 * InjectViewAbility
 *
 * @since 2021-04-26
 */
public class InjectViewAbility extends BaseAbility {

    @Override
    protected int setContentView() {
        setToolbarId(ResourceTable.Id_inject_view_toolbar, StateView.class.getSimpleName());
        return ResourceTable.Layout_ability_inject_view;
    }

    @Override
    protected Component injectTarget() {
        return findComponentById(ResourceTable.Id_text_view);
    }

}
