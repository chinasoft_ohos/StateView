package com.github.nukc.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.utils.Color;

/**
 * MainAbility
 *
 * @since 2021-04-26
 */
public class MainAbility extends Ability {
    private static final int MIN_DELAY_TIME = 1000;
    private static long lastClickTime;

    /**
     * 限制按钮多次点击一秒之内不能重复点击
     *
     * @return boolean 布尔值
     */
    public static boolean isFastClick() {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= MIN_DELAY_TIME) {
            flag = false;
        }
        lastClickTime = currentClickTime;
        return flag;
    }

    private void onClick(String className) {
        if (isFastClick()) {
            return;
        }
        Intent intent1 = new Intent();
        Operation operationBuilder = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(className)
                .build();
        intent1.setOperation(operationBuilder);
        startAbility(intent1);
    }

    private void allClick() {
        findComponentById(ResourceTable.Id_btn_default).setClickedListener(component -> {
            onClick(DefaultAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_inject_ability).setClickedListener(component -> {
            onClick(InjectAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_inject_fragment).setClickedListener(component -> {
            onClick(FragmentAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_custom_view).setClickedListener(component -> {
            onClick(CustomAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_refresh_view).setClickedListener(component -> {
            onClick(RefreshAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_inject_view).setClickedListener(component -> {
            onClick(InjectViewAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_recycler_view).setClickedListener(component -> {
            onClick(InjectRvAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_animation_view).setClickedListener(component -> {
            onClick(AnimatorAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_view_pager2).setClickedListener(component -> {
            onClick(ViewPager2Ability.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_scroll_view).setClickedListener(component -> {
            onClick(ScrollViewAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_set_view).setClickedListener(component -> {
            onClick(SetViewAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_drawer_layout).setClickedListener(component -> {
            onClick(DrawerLayoutAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_wrap_view).setClickedListener(component -> {
            onClick(WrapAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_constraint_layout).setClickedListener(component -> {
            onClick(ConstraintLayoutAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_relative_layout).setClickedListener(component -> {
            onClick(RelativeAbility.class.getName());
        });

        findComponentById(ResourceTable.Id_btn_coordinator_layout).setClickedListener(component -> {
            onClick(CoordinatorLayoutAbility.class.getName());
        });
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        allClick();
    }

}
