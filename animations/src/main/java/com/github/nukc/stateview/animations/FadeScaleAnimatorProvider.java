package com.github.nukc.stateview.animations;

import com.github.nukc.stateview.AnimatorProvider;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;


/**
 * FadeScaleAnimatorProvider
 *
 * @since 2021-04-26
 */
public class FadeScaleAnimatorProvider implements AnimatorProvider {

    @Override
    public Animator showAnimation(Component view) {
        AnimatorProperty animatorProperty = view.createAnimatorProperty();
        animatorProperty.setTarget(view);
        animatorProperty.alpha(1f);
        animatorProperty.scaleX(1f);
        animatorProperty.scaleY(1f);
        return animatorProperty;
    }

    @Override
    public Animator hideAnimation(Component view) {
        final float scaleX = 0.1f;
        final float scaleY = 0.1f;
        AnimatorProperty animatorProperty = view.createAnimatorProperty();
        animatorProperty.setTarget(view);
        animatorProperty.alpha(0f);
        animatorProperty.scaleX(scaleX);
        animatorProperty.scaleY(scaleY);
        return animatorProperty;
    }
}
