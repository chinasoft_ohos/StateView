package com.github.nukc.stateview.animations;


import com.github.nukc.stateview.AnimatorProvider;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

/**
 * FlipAnimatorProvider
 *
 * @since 2021-04-26
 */
public class FlipAnimatorProvider implements AnimatorProvider {


    /** 构造器
     *
     */
    public FlipAnimatorProvider() {
    }

    /** 构造器
     *
     * @param isX 布尔值
     */
    public FlipAnimatorProvider(boolean isX) {
    }

    @Override
    public Animator showAnimation(Component view) {
        final float anim1Alpha = 0.25f;
        final float anim1Rotate = 90f;
        final float anim2Alpha = 0.5f;
        final float anim2Rotate = -15f;
        final float anim3Alpha = 0.75f;
        final float anim3Rotate = 15f;
        final int animatorGroupDuration = 400;
        AnimatorProperty anim1 = new AnimatorProperty(view);
        anim1.alpha(anim1Alpha);
        anim1.rotate(anim1Rotate);
        AnimatorProperty anim2 = new AnimatorProperty(view);
        anim2.alpha(anim2Alpha);
        anim2.rotate(anim2Rotate);
        AnimatorProperty anim3 = new AnimatorProperty(view);
        anim3.alpha(anim3Alpha);
        anim3.rotate(anim3Rotate);
        AnimatorProperty anim4 = new AnimatorProperty(view);
        anim4.alpha(1f);
        anim4.rotate(0f);

        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroup.runSerially(anim1, anim2, anim3, anim4);
        animatorGroup.setDuration(animatorGroupDuration);

        return animatorGroup;
    }

    @Override
    public Animator hideAnimation(Component view) {
        final float anim1Alpha = 1f;
        final float anim1Rotate = -90f;
        final float anim2Alpha = 0.75f;
        final float anim2Rotate = 15f;
        final float anim3Alpha = 0.25f;
        final float anim3Rotate = -15f;
        final int animatorGroupDuration = 400;
        AnimatorProperty anim1 = new AnimatorProperty(view);
        anim1.alpha(anim1Alpha);
        anim1.rotate(anim1Rotate);
        AnimatorProperty anim2 = new AnimatorProperty(view);
        anim2.alpha(anim2Alpha);
        anim2.rotate(anim2Rotate);
        AnimatorProperty anim3 = new AnimatorProperty(view);
        anim3.alpha(anim3Alpha);
        anim3.rotate(anim3Rotate);
        AnimatorProperty anim4 = new AnimatorProperty(view);
        anim4.alpha(0f);
        anim4.rotate(0f);

        AnimatorGroup animatorGroup = new AnimatorGroup();
        animatorGroup.runSerially(anim1, anim2, anim3, anim4);
        animatorGroup.setDuration(animatorGroupDuration);

        return animatorGroup;
    }
}
