## 1.0.0
发布1.0.0正式版本

## 0.0.2-SNAPSHOT
修改findbugs问题

## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为 "传入Ability无法获取顶层component" 原因，" static StateView inject(@NonNull Ability ability, boolean hasActionBar) " api没有实现
 * 因为 "无ActionBar" 原因，" static StateView inject(@NonNull ViewGroup parent, boolean hasActionBar) " api没有实现
 * 因为 "无ActionBar" 原因，" static StateView inject(@NonNull View view, boolean hasActionBar) " api没有实现