# StateView

#### 项目介绍
- 项目名称：StateView
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个轻量级的控件, 当进行操作显示空/重试/加载视图后, 该视图才会被添加到布局中
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v3.0.0

#### 效果演示
![输入图片说明](https://gitee.com/chinasoft_ohos/StateView/raw/master/img/demo.gif "demo.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
	implementation 'com.gitee.chinasoft_ohos:StateView-library:1.0.0'
    implementation 'com.gitee.chinasoft_ohos:StateView-animations:1.0.0'
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

直接在代码中使用:

- 注入到 Component
```java
    mStateView = StateView.inject(Component component);
```

- 注入到 ComponentContainer
```java
    mStateView = StateView.inject(ComponentContainer componentContainer);
```

或添加到布局（这种方式可以更灵活）:

```xml

    <com.github.nukc.stateview.StateView
		ohos:id="$+id:stateView"
		ohos:width="match_parent"
		ohos:height="match_parent" />

```

- 显示空视图: ```mStateView.showEmpty();```
- 显示加载视图: ```mStateView.showLoading();```
- 显示重试视图: ```mStateView.showRetry();```
- 显示内容: ``` mStateView.showContent();```

设置重试点击事件:

```java
    mStateView.setOnRetryClickListener(new StateView.OnRetryClickListener() {
        @Override
        public void onRetryClick() {
            //do something, no need to call showLoading()
            //不需要调用showLoading()方法, StateView自会调用
        }
    });
```

设置自定义视图:

- 全局设置办法:在自己项目的layout下新建, 名字跟StateView默认layout一样即可(也不用代码设置).
默认layout的名字:```base_empty```/```base_retry```/```base_loading```.

- 单页面设置:layout名字不一样, 然后再代码设置.

```java
    setEmptyResource(int emptyResource)

    setRetryResource(int retryResource)

    setLoadingResource(int loadingResource)

```

利用 ```OnInflateListener``` 设置文本图像或者其它操作：
在 view 成功添加到 parent 的时候回调（每个 viewType 只回调一次）

```java
    mStateView.setOnInflateListener(new StateView.OnInflateListener() {
        @Override
        public void onInflate(@StateView.ViewType int viewType, Component view) {
			if (viewType == StateView.EMPTY) {
				// set text or other
				ComponentContainer emptyView = (ComponentContainer) view;
				Text tvMessage = (Text) emptyView.findComponentById(ResourceTable.Id_tv_message);
				Image ivState = (Image) emptyView.findComponentById(ResourceTable.Id_iv_state);
				tvMessage.setText("custom message");
				ivState.setImageAndDecodeBounds(ResourceTable.Media_retry);
			}else if (viewType == StateView.RETRY) {
				//...
			}
        }
    });
```

设置视图切换动画:

```java
    // 默认 provider 是 null，即默认不提供动画切换
    // 如果需要，设置一个就可以了
    setAnimatorProvider(AnimatorProvider provider)

```


目前提供了如下几个动画效果:

- 渐变缩放: ```FadeScaleAnimatorProvider```
- 卡片翻转: ```FlipAnimatorProvider```
- 左右滑动: ```SlideAnimatorProvider```


自定义的话，直接实现 ```AnimatorProvider```接口并提供 ```Animator``` 就可以了

```java
public class FadeScaleAnimatorProvider implements AnimatorProvider {

    @Override
    public Animator showAnimation(Component view) {
        AnimatorProperty animatorProperty = view.createAnimatorProperty();
        animatorProperty.setTarget(view);
        animatorProperty.alpha(1f);
        animatorProperty.scaleX(1f);
        animatorProperty.scaleY(1f);
        return animatorProperty;
    }

    @Override
    public Animator hideAnimation(Component view) {
        AnimatorProperty animatorProperty = view.createAnimatorProperty();
        animatorProperty.setTarget(view);
        animatorProperty.alpha(0f);
        animatorProperty.scaleX(0.1f);
        animatorProperty.scaleY(0.1f);
        return animatorProperty;
    }
}
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代
- 1.0.0

#### 版权和许可信息
```
	The MIT License (MIT)

	Copyright (c) 2016 Nukc

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
```