/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.nukc.stateview.view;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

/**
 * TypedAttrUtils
 *
 * @since 2021-04-26
 */
public final class TypedAttrUtils {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TypedAttrUtils");
    /** 获得颜色的Int值
     *
     * @param attrs 属性集合
     * @param attrName 属性名称
     * @param defValue 定义值
     * @return int 颜色的Int值
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    /** 获得颜色的Color对象
     *
     * @param attrs 属性集合
     * @param attrName 属性名称
     * @param defValue 定义值
     * @return Color 颜色的Color对象
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /** 判断是否为定义值
     *
     * @param attrs 属性集合
     * @param attrName 属性名称
     * @param isDefValue 是否为定义值
     * @return boolean 布尔型
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /** 获取属性string值
     *
     * @param attrs 属性集合
     * @param attrName 属性名称
     * @param defValue 定义值
     * @return String 属性的String值
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /** 获取属性int值
     *
     * @param attrs 属性集合
     * @param attrName 属性名称
     * @param defValue 定义值
     * @return int 属性的int值
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /** 获取属性float值
     *
     * @param attrs 属性集合
     * @param attrName 属性名称
     * @param defValue 定义值
     * @return float 属性的float值
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
            HiLog.info(LABEL, "Exception = " + e.toString());
        }
        return attr;
    }
}
