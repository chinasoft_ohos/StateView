package com.github.nukc.stateview;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

/**
 * AnimatorProvider
 *
 * @since 2021-04-26
 */
public interface AnimatorProvider {

    /** 开启动画
     *
     * @param view 组件
     * @return Animator 动画对象
     */
    Animator showAnimation(Component view);

    /** 隐藏动画
     *
     * @param view 组件
     * @return Animator 动画对象
     */
    Animator hideAnimation(Component view);
}
