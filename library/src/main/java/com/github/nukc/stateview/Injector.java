package com.github.nukc.stateview;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;

/**
 * Injector
 *
 * @since 2021-04-26
 */
class Injector {
    public static void changeChildrenConstraints(ComponentContainer viewParent, StackLayout root, int injectViewId) {
        if (viewParent instanceof ComponentContainer) {
            int rootId = ResourceTable.String_root_id;
            root.setId(rootId);
            ComponentContainer rootGroup = viewParent;
            for (int index = 0, count = rootGroup.getChildCount(); index < count; index++) {
                Component child = rootGroup.getComponentAt(index);
            }
        }
    }
}